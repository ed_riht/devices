const en = {
    appName: "devices",
    device: {
        sections: {
            general: 'General',
            customer: 'Customer',
            warranty: 'Warranty',
        },
        fields: {
            deviceId: 'Device ID',
            partNumber: 'Part Number',
            manufacturingDate: 'Manufacturing date',
            customerType: 'Customer type',
            customer: 'Customer',
            deviceGroup: 'Device Group',
            warrantyDate: 'Warranty date',
            warrantyDuration: 'Warranty duration',
        },
        customerTypes: {
            organization: 'Organization',
            consumer: 'Consumer'
        },
        actions: {
            save: 'Save Device',
            cancel: 'Cancel',
            add: 'Add Device'
        },
        names: {
            module: 'Devices',
            create: 'Create Device',
        }
    },
    toast: {
        success: {
            message: 'Success',
            description: 'Action was executed successfully'
        },
    }
};

export default en;
