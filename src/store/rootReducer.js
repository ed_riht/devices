import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';

import { reducer as coreReducer, constants as coreConstants } from '../modules/core';
import { reducer as devicesReducer, constants as devicesConstants } from '../modules/devices';

const ROUTER_NAME = 'router';

const persistConfig = {
    key: 'root',
    storage,
    blacklist: [
        ROUTER_NAME,
        coreConstants.NAME,
        devicesConstants.NAME,
    ]
};

const rootReducer = combineReducers({
    [ROUTER_NAME]: routerReducer,
    [coreConstants.NAME]: coreReducer,
    [devicesConstants.NAME]: devicesReducer,
});

if (module.hot) {
    module.hot.accept(() => {
        storage.replaceReducer(
            persistReducer(persistConfig, rootReducer)
        )
    })
}

export default rootReducer;
