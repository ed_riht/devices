const colors = {
    transparent: "rgba(0,0,0,0)",
    white: "#fff",
    black: "#13120f",
    primary: '#2462ff',
    blue: {
        blue: "#1976d2",
        darkBlue: "#3c4858",
        primary: '#3f51b5',
    },
    gray: {
        lightGray: "#b4b4b4",
        gray: "#eeeeee",
        darkGray: "#555555",
        transGray: '#92929259',
        transTransGray: '#9292921f',
        mediumGray: '#818181',
    },
};

export default colors;
