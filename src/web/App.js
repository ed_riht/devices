import React, { PureComponent } from 'react';
import { ToastContainer } from 'react-toastify';
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from 'react-redux';
import { I18nextProvider } from 'react-i18next';
import { store, persistor } from '../store';
import i18n from '../i18n/i18n';
import Router from './Router';
import { history } from '../modules/core';


class App extends PureComponent {
    render() {
        return (
            <I18nextProvider i18n={i18n}>
                <Provider store={store}>
                    <>
                        <PersistGate loading={null} persistor={persistor}>
                            <Router history={history} />
                        </PersistGate>
                        <ToastContainer hideProgressBar/>
                    </>
                </Provider>
            </I18nextProvider>
        )
    }
}

export default App;
