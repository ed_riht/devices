import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Route, Redirect, Switch } from "react-router-dom";
import { ConnectedRouter } from "react-router-redux";
import { LastLocationProvider } from "react-router-last-location";
import { connect } from "react-redux";
import _ from "lodash";

import { components as Shared, constants as sharedConstants } from "../modules/shared";
import { constants as coreConstants, components as Core, translate } from "../modules/core";


class Router extends PureComponent {
    static propTypes = {
        i18n: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
    };

    get renderRoutes() {
        return (
            <Shared.Root>
                <Switch>
                    {_.map(sharedConstants.loggedOutRoutes, this.renderRoute)}
                    <Redirect to={coreConstants.ROUTES.devices()} />
                </Switch>
            </Shared.Root>
        );
    }

    renderComponent = (item, customProps) => (<Core.LoadingComponent item={item} customProps={customProps} />);

    renderRoute = item => (
        <Route
            key={item.route}
            exact={true}
            path={item.route}
            render={props => this.renderComponent(item, props)}
        />
    );

    render() {
        return (
            <ConnectedRouter history={this.props.history}>
                <LastLocationProvider>
                    {this.renderRoutes}
                </LastLocationProvider>
            </ConnectedRouter>
        );
    }
}

export default connect()(translate()(Router));
