import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import '../styles/common.css';
import 'react-toastify/dist/ReactToastify.css';

ReactDOM.render(
    <App />,
    document.getElementById('app')
);

module.hot.accept();
