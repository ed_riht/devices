import { NAME, LOADING_NAME } from './constants';

export const isLoading = (state, actionType) => state[NAME][LOADING_NAME][actionType];
