export default function createActionTypes(actionType) {
    return {
        NAME: actionType,
        REQUEST: `${actionType}_REQUEST`,
        SUCCESS: `${actionType}_SUCCESS`,
        ERROR: `${actionType}_ERROR`
    };
}