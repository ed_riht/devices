import { NAME } from './constants';

export const RESET = `${NAME}/RESET`;
export const ADD_ERROR = `${NAME}/ADD_ERROR`;
export const REMOVE_ERROR = `${NAME}/REMOVE_ERROR`;