export const NAME = 'core';
export const LOADING_NAME = 'loading';
export const ERROR_NAME = 'error';

export const ROUTES = {
    devicesForm: () => '/deviceform',
    devices: () => '/devices'
};

export const INPUT_TYPES = {
    text: 'text',
    password: 'password',
    email: 'email',
    number: 'number',
    select: 'select',
    date: 'date'
};
