import React from 'react';
import _ from 'lodash';
import { makeStyles } from "@material-ui/core";
import { colors, fonts, spacing } from '../../../../styles';

const useStyles = makeStyles({
    text: {
        paddingBottom: spacing.s4,
        color: colors.primary,
        ...fonts.smNormal,
        cursor: 'pointer',
    },
});

const getPathString = items => {
    let result = '';

    if (items && items.length > 1) {
        _.map(items, (item, index, arr) => {
            result = arr.length === index + 1 ? `${result}${item}` : `${result}${item}  /  `
        })
    }

    return result
};

const Path = ({ items, isSingle }) => {
    const classes = useStyles();
    const value = getPathString(items);
    return (
        <div className={classes.text}>
            {value && !isSingle ? (
                <span>{value}</span>
            ) : (
                null
            )}
        </div>
    ) };

export default Path;