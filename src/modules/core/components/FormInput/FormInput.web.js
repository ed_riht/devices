import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import TextField from '@material-ui/core/TextField';
import FormControl from "@material-ui/core/FormControl/FormControl";
import { colors } from "../../../../styles";

class FormInput extends React.PureComponent {
    static propTypes = {
        key: PropTypes.string,
        margin: PropTypes.string,
        required: PropTypes.bool,
        fullWidth: PropTypes.bool,
        htmlFor: PropTypes.string.isRequired,
        inputLabel: PropTypes.string,
        onChange: PropTypes.func,
        type: PropTypes.string,
        autoFocus: PropTypes.bool,
        startAdornment: PropTypes.string,
        endAdornment: PropTypes.string,
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        disabled: PropTypes.bool,
        className: PropTypes.string,
        inputProps: PropTypes.object,
        labelClassName: PropTypes.string,
        isError: PropTypes.bool,
    };

    static defaultProps = {
        key: '',
        margin: "normal",
        isError: false,
        required: false,
        fullWidth: true,
        inputLabel: '',
        autoFocus: false,
        disabled: false,
        defaultValue: '',
        type: 'text',
        startAdornment: null,
        value: undefined,
        onChange: undefined,
        className: undefined,
        labelClassName: undefined,
        endAdornment: undefined,
        inputProps: undefined,
    };

    render() {
        const {
            margin,
            required,
            fullWidth,
            htmlFor,
            inputLabel,
            onChange,
            type,
            autoFocus,
            disabled,
            className,
            value,
        } = this.props;
        return (
            <FormControl key={`${htmlFor}-control`} margin={margin} required={required} fullWidth={fullWidth} disabled={disabled} className={className}>
                <TextField
                    id={`${htmlFor}-input`}
                    label={inputLabel}
                    name={htmlFor}
                    autoFocus={autoFocus}
                    onChange={onChange}
                    type={type}
                    value={value}
                    required={required}
                />
            </FormControl>
        );
    }
}

const styles = () => ({
    typography: {
        useNextVariants: true,
    },
    overrides: {
        MuiFormLabel: {
            root: {
                "&$focused": {
                    color: `${colors.blue.blue} !important`
                }
            }
        },
        MuiInput: {
            underline: {
                "&:after": {
                    borderColor: `${colors.blue.blue} !important`
                },
                "&:hover": {
                    "&:before": {
                        borderBottom: `2px solid ${colors.blue.blue} !important`,
                    },
                },
            },
        }
    }
});


export default withStyles(styles)(FormInput);
