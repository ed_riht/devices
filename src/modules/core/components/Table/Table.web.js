import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import _ from 'lodash';
import NativeTable from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    root: {
        width: '100%',
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
});

const formatDate = date => {
    const day = date.getDate();
    const monthIndex = date.getMonth();
    const year = date.getFullYear();

    return `${day}/${monthIndex + 1}/${year}`;
};

const getTableRow = (item, fields) => (
    <TableRow key={item.deviceId}>
        {_.map(fields, i => {
            let value = item[i];
            if (_.isDate(value)) {
                value = formatDate(value);
            }
            return (<TableCell>{value}</TableCell>)
        })}
    </TableRow>
);

const getTableHeader = (item, i18n) => (<TableCell>{i18n.t(`device.fields.${item}`)}</TableCell>);

export default function Table({ data, fields, i18n }) {
    const classes = useStyles();

    return (
        <Paper className={classes.root}>
            <NativeTable className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        {_.map(fields, item => getTableHeader(item, i18n))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {_.map(data, item => getTableRow(item, fields))}
                </TableBody>
            </NativeTable>
        </Paper>
    );
}