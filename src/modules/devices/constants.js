import { constants as coreConstants } from "../core";

export const NAME = 'devices';

export const FIELDS = {
    deviceId: 'deviceId',
    partNumber: 'partNumber',
    manufacturingDate: 'manufacturingDate',
    customerType: 'customerType',
    customer: 'customer',
    deviceGroup: 'deviceGroup',
    warrantyDate: 'warrantyDate',
    warrantyDuration: 'warrantyDuration',
};

export const SECTIONS = {
    general: 'general',
    customer: 'customer',
    warranty: 'warranty',
};

export const DEVICE_FORM = [{
    name: FIELDS.deviceId,
    required: true,
    type: coreConstants.INPUT_TYPES.text,
    section: SECTIONS.general,
}, {
    name: FIELDS.partNumber,
    required: true,
    type: coreConstants.INPUT_TYPES.text,
    section: SECTIONS.general,
}, {
    name: FIELDS.manufacturingDate,
    required: true,
    type: coreConstants.INPUT_TYPES.date,
    section: SECTIONS.general,
}, {
    name: FIELDS.customerType,
    type: coreConstants.INPUT_TYPES.number,
    section: SECTIONS.customer,
}, {
    name: FIELDS.customer,
    type:  coreConstants.INPUT_TYPES.select,
    section: SECTIONS.customer,
}, {
    name: FIELDS.deviceGroup,
    type: coreConstants.INPUT_TYPES.select,
    section: SECTIONS.customer,
}, {
    name: FIELDS.warrantyDate,
    type: coreConstants.INPUT_TYPES.date,
    section: SECTIONS.warranty,
}, {
    name: FIELDS.warrantyDuration,
    type: coreConstants.INPUT_TYPES.select,
    section: SECTIONS.warranty,
}];

export const CUSTOMER_TYPES = {
    consumer: {
        name: 'consumer',
        value: 0
    },
    organization: {
        name: 'organization',
        value: 1,
    }
};

//MOCKS

export const SELECT_OPTIONS_MOCK = {
    [FIELDS.customer]: [{
        id: 'd0ca7605-55c1-4286-a78d-96ad85919927',
        name: 'Customer1',
    }, {
        id: '77379c7e-3749-40d4-9d16-21f9028d9cb7',
        name: 'Customer2',
    }],
    [FIELDS.deviceGroup]: [{
        id: 'fd488016-b3f9-404e-8dd7-f155898ffa87',
        name: 'Ebay',
    }, {
        id: '40978290-33d2-4f11-aa17-acd757a5c267',
        name: 'Kebab'
    }],
    [FIELDS.warrantyDuration]: [{
        id: '84998837-b1be-4baf-847c-00f1a353ea57',
        name: 'Warranty 1',
    }, {
        id: 'e75e049f-ab1e-4b38-9190-0d37e706b677',
        name: 'Warranty 2'
    }]
};
