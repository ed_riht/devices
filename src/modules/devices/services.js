import _ from 'lodash';

export const validateData = (item, fields) => {
    let validate = false;

    _.forOwn(item, (value, key) => {
        const field = _.find(fields, i => i.name === key);
        if (field && field.required) {
            if (!value) {
                validate = true;
            }
        }
    });

    return validate;
};