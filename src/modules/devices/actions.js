import * as actionTypes from "./actionTypes";
import { Toast } from '../core';

export function createDeviceItem(data) {
    return function (dispatch) {
        Toast.success();
        dispatch({ type: actionTypes.CREATE_DEVICE_ITEM.SUCCESS, payload: { data } });
    }
}