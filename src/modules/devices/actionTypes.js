import { NAME } from './constants';
import { createActionType } from '../core';

export const CREATE_DEVICE_ITEM = createActionType(`${NAME}/CREATE_DEVICE_ITEM`);