import React from 'react';
import PropTypes from 'prop-types';
import withDevicesBase from './Devices.base';
import DeviceForm from '../DeviceForm';
import DevicesTable from '../DevicesTable';
import { components as Core, withStyles } from "../../../core";
import { colors, spacing } from "../../../../styles";

class Devices extends React.PureComponent {
    static propTypes = {
        i18n: PropTypes.object.isRequired,
        classes: PropTypes.object.isRequired,
        isFormVisible: PropTypes.bool.isRequired,
        onChangeFormState: PropTypes.func.isRequired
    };

    render() {
        const { classes, isFormVisible, i18n, onChangeFormState } = this.props;
        return (
            <div>
                {/* Ugly way but fastest ;) */}
                <Core.Path
                    items={[
                        i18n.t('device.names.module'),
                        i18n.t('device.names.create')
                    ]}
                    isSingle={!isFormVisible}
                />
                {isFormVisible ?
                    null : (
                        <Core.Button
                            className={classes.button}
                            variant="contained"
                            size="medium"
                            onClick={onChangeFormState}>
                            {i18n.t('device.actions.add')}
                        </Core.Button>
                    )}
                {
                    isFormVisible ?
                        <DeviceForm onChangeFormState={onChangeFormState} /> :
                        <DevicesTable />
                }
            </div>
        )
    }
}

const styles = () => ({
    button: {
        backgroundColor: colors.primary,
        color: colors.white,
        marginBottom: spacing.s3
    }
});

export default withDevicesBase(withStyles(styles)(Devices));
