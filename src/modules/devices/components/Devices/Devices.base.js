import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { translate, constants as coreConstants } from "../../../core";
import * as actions from "../../actions";

export default function withDevicesBase(WrappedComponent) {
    class DevicesBase extends React.PureComponent {
        static propTypes = {
            i18n: PropTypes.object.isRequired,
            actions: PropTypes.object.isRequired,
        };

        constructor(props) {
            super(props);

            this.state = {
                isFormVisible: false,
            }
        }

        onChangeFormState = () => this.setState(prevState => ({ isFormVisible: !prevState.isFormVisible }));

        render() {
            return (
                <WrappedComponent
                    {...this.props}
                    isFormVisible={this.state.isFormVisible}
                    onChangeFormState={this.onChangeFormState}
                />
            );
        }
    }

    function mapStateToProps(state) {
        return {

        }
    }

    function mapDispatchToProps(dispatch) {
        return {
            actions: bindActionCreators(actions, dispatch)
        }
    }

    return connect(mapStateToProps, mapDispatchToProps)(translate()(DevicesBase));
}
