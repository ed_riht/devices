import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Schedule from '@material-ui/icons/Schedule';
import InsertInvitation from '@material-ui/icons/InsertInvitation';
import PermIdentity from '@material-ui/icons/PermIdentity';
import withDeviceFormBase from './DeviceForm.base';
import { withStyles, components as Core, constants as coreConstants } from "../../../core"
import { DEVICE_FORM, SECTIONS } from "../../constants";
import { spacing, fonts, colors } from '../../../../styles';


class DeviceForm extends React.PureComponent {
    static propTypes = {
        i18n: PropTypes.object.isRequired,
        classes: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.string]).isRequired,
        selectorItems: PropTypes.object.isRequired,
        customerTypes: PropTypes.array.isRequired,
        onChangeValue: PropTypes.func.isRequired,
        values: PropTypes.object.isRequired,
        isDisabledSaveBtn: PropTypes.bool.isRequired,
        onCancel: PropTypes.func.isRequired,
        onSave: PropTypes.func.isRequired,
    };

    get form() {
        return _.map(SECTIONS, this.getSection);
    }

    get actionSection() {
        const { i18n, classes, onCancel, isDisabledSaveBtn, onSave } = this.props;
        return (
            <Core.Grid item={true} xs={12} sm={12}>
                <Core.Button
                    className={classes.button}
                    disabled={isDisabledSaveBtn}
                    variant="contained"
                    size="medium"
                    onClick={onSave}>
                    {i18n.t('device.actions.save')}
                </Core.Button>
                <Core.Button
                    className={classes.cancelBtn}
                    size="medium"
                    onClick={onCancel}>
                    {i18n.t('device.actions.cancel')}
                </Core.Button>
            </Core.Grid>
        )
    }

    getSectionIcon = sectionName => {
        if (SECTIONS.warranty === sectionName) {
            return <Schedule fontSize="large" />
        }

        if (SECTIONS.customer === sectionName) {
            return <PermIdentity fontSize="large" />
        }

        return <InsertInvitation fontSize="large" />
    };

    getSection = sectionName => {
        const { classes } = this.props;
        const icon = this.getSectionIcon(sectionName);
        return (
            <>
                <Core.Grid item={true} xs={12} sm={12}>
                    <div className={classes.sectionHeader}>
                        {icon}
                        <span className={classes.sectionHeaderLabel}>{this.props.i18n.t(`device.sections.${sectionName}`)}</span>
                    </div>
                </Core.Grid>
                {
                    _.chain(DEVICE_FORM)
                        .filter(i => i.section === sectionName)
                        .map(this.getFormItem)
                        .value()
                }
            </>
        )
    };

    getFormItemElement = item => {
        const { i18n, selectorItems, customerTypes, onChangeValue, values } = this.props;
        const baseProps = {
            inputLabel: i18n.t(`device.fields.${item.name}`),
            required: item.required,
        };

        if (item.type === coreConstants.INPUT_TYPES.date) {
            return (
                <Core.DatePicker
                    {...baseProps}
                    onChange={date => onChangeValue(item.name, date, item.type)}
                    value={values[item.name]}
                />
            )
        }
        if (item.type === coreConstants.INPUT_TYPES.select) {
            return (
                <Core.FormSelect
                    {...baseProps}
                    items={selectorItems[item.name]}
                    htmlFor={item.name}
                    onChange={event => onChangeValue(item.name, event.target.value, item.type)}
                />
            )
        }
        if (item.type === coreConstants.INPUT_TYPES.number) {
            return (
                <Core.RadioComponent
                    items={customerTypes}
                    title={i18n.t('device.fields.customerType')}
                    value={values[item.name]}
                    onChange={event => onChangeValue(item.name, event.target.value, item.type)}
                />
            )
        }

        return (
            <Core.FormInput
                {...baseProps}
                onChange={event => onChangeValue(item.name, event.target.value, item.type)}
                type={coreConstants.INPUT_TYPES.text}
                value={values[item.name]}
            />
        )
    };

    getFormItem = item => (
        <Core.Grid item={true} xs={12} sm={4} alignItems="center" className={this.props.classes.container}>
            {this.getFormItemElement(item)}
        </Core.Grid>
    );

    render() {
        return (
            <Core.Grid container={true} spacing={2}>
                {this.form}
                {this.actionSection}
            </Core.Grid>
        );
    }
}

const styles = () => ({
    sectionHeader: {
        display: 'flex',
        alignItems: 'center'
    },
    sectionHeaderLabel: {
        marginLeft: spacing.s2,
        ...fonts.lgLight
    },
    container: {
        padding: `${spacing.s2} ${spacing.s9} ${spacing.s9} ${spacing.s9} !important`
    },
    cancelBtn: {
        marginLeft: spacing.s2,
        color: colors.primary
    },
    button: {
        backgroundColor: colors.primary,
        color: colors.white,
    }
});

export default withDeviceFormBase(withStyles(styles)(DeviceForm));
