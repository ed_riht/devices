import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { translate, constants as coreConstants } from "../../../core";
import * as actions from "../../actions";
import { validateData } from '../../services';
import { getDevices } from '../../selectors';
import { SELECT_OPTIONS_MOCK, CUSTOMER_TYPES, FIELDS, DEVICE_FORM } from '../../constants';

const initialState = {
    [FIELDS.deviceId]: null,
    [FIELDS.partNumber]: null,
    [FIELDS.manufacturingDate]: new Date(),
    [FIELDS.customerType]: 0,
    [FIELDS.customer]: null,
    [FIELDS.deviceGroup]: null,
    [FIELDS.warrantyDate]: new Date(),
    [FIELDS.warrantyDuration]: null,
};

export default function withDeviceFormBase(WrappedComponent) {
    class DeviceFormBase extends React.PureComponent {
        static propTypes = {
            i18n: PropTypes.object.isRequired,
            actions: PropTypes.object.isRequired,
            onChangeFormState: PropTypes.func.isRequired,
        };

        constructor(props) {
            super(props);

            this.state = initialState;
        }

        get customerTypes() {
            return _.map(CUSTOMER_TYPES, item => ({
                value: item.value,
                label: this.props.i18n.t(`device.customerTypes.${item.name}`)
            }))
        }

        get isDisabledSaveBtn() {
            return validateData(this.state, DEVICE_FORM);
        }

        onCancel = () => {
            this.setState(() => ({ ...initialState }), this.props.onChangeFormState);
        };

        onChangeValue = (name, value, type) => {
            const formattedValue = type === coreConstants.INPUT_TYPES.number ? parseInt(value) : value;
            this.setState(prevState => ({
                ...prevState,
                [name]: formattedValue,
            }))
        };

        onSave = () => {
            const { actions, onChangeFormState } = this.props;

            actions.createDeviceItem(this.state);
            setTimeout(onChangeFormState, 1000);
        };

        render() {
            return (
                <WrappedComponent
                    {...this.props}
                    customerTypes={this.customerTypes}
                    onChangeValue={this.onChangeValue}
                    values={this.state}
                    onCancel={this.onCancel}
                    isDisabledSaveBtn={this.isDisabledSaveBtn}
                    onSave={this.onSave}
                />
            );
        }
    }

    function mapStateToProps(state) {
        return {
            selectorItems: SELECT_OPTIONS_MOCK,
            devices: getDevices(state)
        }
    }

    function mapDispatchToProps(dispatch) {
        return {
            actions: bindActionCreators(actions, dispatch)
        }
    }

    return connect(mapStateToProps, mapDispatchToProps)(translate()(DeviceFormBase));
}
