import React from 'react';
import PropTypes from 'prop-types';
import withDevicesTableBase from './DevicesTable.base';
import { components as Core } from '../../../core';
import { FIELDS } from "../../constants";

class DevicesTable extends React.PureComponent {
    static propTypes = {
        i18n: PropTypes.object.isRequired,
        devices: PropTypes.array.isRequired,
    };

    render() {
        return (
            <Core.Table fields={FIELDS} i18n={this.props.i18n} data={this.props.devices} />
        );
    }
}

export default withDevicesTableBase(DevicesTable);
