import { NAME } from './constants';

export const getDevices = state => state[NAME].devices;