import * as actionTypes from './actionTypes';
import storage from "redux-persist/es/storage";
import { persistReducer } from "redux-persist";
import { NAME } from './constants';

const initialState = {
    devices: [],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CREATE_DEVICE_ITEM.SUCCESS: {
            const { data } = action.payload;
            return {
                ...state,
                devices: [
                    ...state.devices,
                    data,
                ]
            }
        }
        default:
            return state;
    }
};

const persistConfig = {
    key: NAME,
    storage,
    whitelist: ['devices']
};

export default persistReducer(persistConfig, reducer);