import React from 'react';
import { constants as coreConstants } from "../core";

export const NAME = "share";

export const loggedOutRoutes = [
    {
        route: coreConstants.ROUTES.devices(),
        component: React.lazy(() => import('../devices/components/Devices'))
    },
    {
        route: coreConstants.ROUTES.devices(),
        component: React.lazy(() => import('../devices/components/Devices'))
    },
];


export const ALLOWED_ROUTES = () => {
    return loggedOutRoutes;
};

export const DRAWER_WIDTH = 260;
export const APPBAR_HEIGHT = 40;
