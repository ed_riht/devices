import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { withStyles, translate, history } from '../../../core';
import { colors, spacing, hexToRGB } from "../../../../styles";
import Header from "../Header";

import { APPBAR_HEIGHT } from "../../constants";


class Root extends React.PureComponent {
    static propTypes = {
        children: PropTypes.element.isRequired,
        i18n: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
        classes: PropTypes.object.isRequired,
    };

    render() {
        const { classes, children } = this.props;
        return (
            <div className={classes.wrapper}>
                <Header />
                <div className={classes.content}>
                    <div className={classes.container}>
                        {children}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
    }
}

const HEADER_MARGIN = APPBAR_HEIGHT + spacing.s2;

const styles = theme => ({
    wrapper1: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: `rgba(${hexToRGB(colors.gray.lightGray)} , 0.15)`,
        color: colors.blue.darkBlue,
    },
    wrapper: {
        position: "relative",
        float: "right",
        transition: "all 0.33s cubic-bezier(0.685, 0.0473, 0.346, 1)",
        maxHeight: "100%",
        width: "100%",
        overflowScrolling: "touch",
        backgroundColor: `rgba(${hexToRGB(colors.gray.lightGray)} , 0.15)`,
        color: colors.blue.darkBlue,
    },
    content: {
        marginTop: HEADER_MARGIN,
        padding: `${spacing.s11}px ${spacing.s11}px`,
        minHeight: `calc(100vh - 160px)`
    },
    container: {
        paddingRight: spacing.s3,
        paddingLeft: spacing.s3,
    }
});

export default withRouter(
    connect(mapStateToProps)(
        translate()(
            withStyles(styles)(Root)
        )
    )
);
