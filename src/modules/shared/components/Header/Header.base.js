import React from 'react';
import { connect } from "react-redux";
import { translate } from '../../../core';

export default function withHeaderBase(WrappedComponent) {
    class HeaderBase extends React.PureComponent {
        render() {
            return (
                <WrappedComponent
                    {...this.props}
                />
            );
        }
    }

    function mapStateToProps(state) {
        return {
        }
    }

    function mapDispatchToProps(dispatch) {
        return {
        }
    }

    return connect(mapStateToProps, mapDispatchToProps)(translate()(HeaderBase));
}
