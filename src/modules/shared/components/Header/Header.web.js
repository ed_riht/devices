import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import className from "classnames";
import logo from "../../../../../assets/img/Logo.jpg";
import { spacing, colors, hexToRGB, fonts } from '../../../../styles';
import { components as Core, withStyles, translate } from "../../../core";
import { APPBAR_HEIGHT } from "../../constants";
import withHeaderBase from "./Header.base";

class Header extends React.PureComponent {
    static propTypes = {
        history: PropTypes.object.isRequired,
        i18n: PropTypes.object.isRequired,
        classes: PropTypes.object.isRequired,
    };

    get logo() {
        const { classes } = this.props;
        return (
            <div className={classes.logo}>
                <a href="#" className={classes.logoLink}>
                    <div className={classes.logoImage}>
                        <img src={logo} alt="logo" className={classes.img} />
                    </div>
                </a>
            </div>
        );
    }

    render() {
        const { classes } = this.props;
        return (
            <Core.AppBar className={className(classes.appBar)}>
                <Core.Toolbar className={classes.container}>
                    <div className={className(classes.flex, classes.title)}>
                        {this.logo}
                    </div>
                </Core.Toolbar>
            </Core.AppBar>
        );
    }
}

const APPBAR_SHADOW = `0 12px 20px -10px rgba(${hexToRGB(colors.blue.blue)},.28), 0 4px 20px 0 rgba(${hexToRGB(colors.black)} ,.12), 0 7px 8px -5px rgba(${hexToRGB(colors.blue.blue)}, .2)`;
const LOGO_IMAGE = 75;
const IMAGE = 75;
const FONT_SIZE_LOGO = 30;

const styles = theme => ({
    appBar: {
        backgroundColor: colors.black,
        boxShadow: "none",
        position: "absolute",
        width: "100%",
        paddingTop: spacing.s1,
        zIndex: "1029",
        color: colors.gray.darkGray,
        borderRadius: "3px",
        padding: `${spacing.s2} 0`,
        transition: "all 150ms ease 0s",
        minHeight: APPBAR_HEIGHT,
        display: "block",
        maxHeight: 70
    },
    container: {
        paddingRight: spacing.s3,
        paddingLeft: spacing.s3,
        marginRight: "auto",
        marginLeft: "auto"
    },
    flex: {
        flex: 1
    },
    title: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    appBarTheme: {
        backgroundColor: colors.blue.blue,
        boxShadow: APPBAR_SHADOW,
        "&:hover": {
            backgroundColor: colors.blue,
            boxShadow: APPBAR_SHADOW
        }
    },
    logo: {
        position: "relative",
        zIndex: 4,
    },
    logoLink: {
        padding: `${spacing.s1}px 0`,
        textDecoration: "none",
        display: "flex",
        alignItems: "center",
        ...fonts.mdNormal,
        fontSize: FONT_SIZE_LOGO,
        "&,&:hover": {
            color: colors.white
        }
    },
    logoImage: {
        width: LOGO_IMAGE,
        display: "inline-block",
        maxHeight: LOGO_IMAGE,
        marginLeft: spacing.s1,
        marginRight: spacing.s3
    },
    img: {
        width: IMAGE,
        top: spacing.s5,
        verticalAlign: "middle",
        border: 0
    },
});


export default withRouter(withHeaderBase(translate()(withStyles(styles)(Header))));
