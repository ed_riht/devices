Simple app with mocks and quick resolutions :)

## Available Scripts

In the project directory, you can run:

### `yarn web`

Runs the app in the development mode.<br />
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

### `yarn build`